call pathogen#infect()
source ~/.vim/rc/vimrc72.vim
source ~/.vim/rc/backup.vim
source ~/.vim/rc/swap.vim
source ~/.vim/rc/viminfo.vim
source ~/.vim/rc/search-multilines.vim
source ~/.vim/rc/colors-solarized.vim
source ~/.vim/rc/listchars-rightquotation-tilda-dollar.vim
source ~/.vim/rc/set-font.vim
set list
set number
set laststatus=2
set cmdheight=2
set backspace=eol,start,indent
map <Esc>[A <Up>
map <Esc>[B <Down>
map <Esc>[C <Right>
map <Esc>[D <Left>
cmap <Esc>[A <Up>
cmap <Esc>[B <Down>
cmap <Esc>[C <Right>
cmap <Esc>[D <Left>
