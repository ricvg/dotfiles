source ~/.vim/rc/termColors.vim
if &t_Co>=256
	colorscheme xoria256
else
	colorscheme desert
	hi LineNr ctermfg=darkgrey
	hi SpecialKey ctermfg=darkgrey
endif
