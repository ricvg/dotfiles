"source ~/.vim/rc/termColors.vim
" Don't use solarized in a windows console.
"if &term != "win32"
	set background=dark
	let g:solarized_termcolors=256
	let g:solarized_visibility="low"
	colorscheme solarized
"endif
