
" Use Ctrl-V u 00b7 for · (middle dot).
" Use Ctrl-V u 00bb for » (right-pointing double angle quotation mark).

if has("unix")
	if "SunOS\n" == system("uname")
		set listchars=tab:>-,trail:~,extends:$
	else
		scriptencoding utf-8
		set encoding=utf-8
		set listchars=tab:»·,trail:~,extends:»,precedes:«
	endif
else
	" Probably windows
	scriptencoding utf-8
	set encoding=utf-8
	set listchars=tab:��,trail:~,extends:�,precedes:�
endif
