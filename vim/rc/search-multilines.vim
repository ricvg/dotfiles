" Properly search over visually selected text over multiple lines.
vmap * y/\V<C-r>=substitute(escape(@@,"/\\"),"\n","\\n","ge")<CR><CR>
vmap # y?\V<C-r>=substitute(escape(@@,"?\\"),"\n","\\n","ge")<CR><CR>
vmap <kMultiply> y/\V<C-r>=substitute(escape(@@,"/\\"),"\n","\\\\n","ge")<CR><CR>
