:: RICVG dotfiles set up for NTFS (Win Vista and up)
set __DOTFILES=%~dp0
set __HOME=%HOMEDRIVE%%HOMEPATH%

:: Vim set up.
set __VIMRC=%__HOME%\_vimrc
set __DOTVIM=%__HOME%\.vim
set __VIMFILES=%__HOME%\vimfiles
if NOT exist "%__VIMRC%"  mklink %__VIMRC% %__DOTFILES%\vim\default.vim
if NOT exist "%__DOTVIM%"  mklink /d %__DOTVIM% %__DOTFILES%\vim
if NOT exist "%__VIMFILES%"  mklink /d %__VIMFILES% %__DOTFILES%\vim

:: Delete variables
set __DOTFILES=
set __HOME=
set __VIMRC=
set __DOTVIM=
set __VIMFILES=
