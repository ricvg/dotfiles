#!/bin/sh

usage() {
	echo "Usage $0 [-p <hostname_prefix>] [-d <domain>]" 1>&2; exit 1;
}
#..............................................................................
get_options() {
	while getopts ":d:p:" optname
	do
		case $optname in
			d)
				opt_domainToRemvoe=${OPTARG}
				;;
			p)
				opt_prefixToRemove=${OPTARG}
				;;
			*)
				usage
				;;
		esac
	done
}
#..............................................................................
remove_prefix() {
	local input="$1"
	local prefix="$2"

	if [ ! -z "${prefix}" ]; then
		echo $(echo $input | sed "s/^${prefix}//")
	else
		echo "$input"
	fi
}
#..............................................................................
remove_domain() {
	local input="$1"
	local domain="$2"

	if [ ! -z "${domain}" ]; then
		echo $(echo $input | sed "s/\.*${domain}.*$//")
	else
		echo "$input"
	fi
}
#..............................................................................
SHORTHOST=$(echo ${HOSTNAME} )
get_options "${@}"
SHORTHOST=$(remove_prefix ${SHORTHOST} "${opt_prefixToRemove}")
SHORTHOST=$(remove_domain ${SHORTHOST} "${opt_domainToRemvoe}")
export SHORTHOST

