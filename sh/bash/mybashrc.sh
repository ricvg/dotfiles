#!/bin/bash
#..............................................................................
opt_d_dullprompt=1
opt_perforce_prompt=1
opt_use_tmux=1
get_options() {
	while getopts ":d4t" optname
	do
		case $optname in
			d)
				opt_d_dullprompt=0
				;;
			4)
				opt_perforce_prompt=0
				;;
			t)
				opt_use_tmux=0
				;;
			*)
				echo "Unknown option $optname"
				;;
		esac
	done
}
#..............................................................................
is_dullprompt_requested() {
	if [ $opt_d_dullprompt -eq 0 ];
	then
		return 0
	fi

	return 1
}
#..............................................................................
is_perforceprompt_requested() {
	if [ $opt_perforce_prompt -eq 0 ];
	then
		return 0
	fi

	return 1
}
#..............................................................................
is_tmux_requested() {
	if [ $opt_use_tmux -eq 0 ];
	then
		return 0
	fi

	return 1
}
#..............................................................................
echo_absolute_path() {
	local path="$1"

	while [ -L "$path" ]; do
		dir=`dirname "$path"`
		path=`ls -l "$path" | sed -e 's/.* -> //'`
		cd "$dir"
	done

	local file=`basename "$path"`
	local dir=""

	if [ ! -d "$file" ] ; then
		# $file is NOT a directory, then split the path into dir and file.
		dir=`dirname "$path"`
	else
		# $file is a directory, then clear $file since is is not useful.
		file=""
		dir="$path"
	fi

	if [ ! -d "$dir" ]; then
		return 1
	fi

	local abs_name="`cd "$dir" && pwd -P`"

	if [ -n "$file" ]; then
		abs_name=$abs_name/$file
	fi
	echo $abs_name
}
#..............................................................................
RICVG_DOTFILES_DIR=''
get_dotfiles_dir() {
	local script_path=`echo_absolute_path "${BASH_SOURCE[0]}"`
	local dotfiles_rel_path=`dirname "$script_path"`/../..
	local dotfiles_path=`echo_absolute_path "$dotfiles_rel_path"`
	RICVG_DOTFILES_DIR=$dotfiles_path
	export RICVG_DOTFILES_DIR
}
#..............................................................................
fancyprompt() {
	if $(util_user_is_root);
	then # you are root, set red colour prompt
		PS1="$(util_fgcolor 88)\u@$SHORTHOST \W# $(util_resetcolor)"
	else
		PS1="$(util_fgcolor 243)\u@$SHORTHOST \W\$ $(util_resetcolor)"
	fi
}
#..............................................................................
dullprompt() {
	PROMPT_COMMAND=""
	if $(util_user_is_root);
	then # you are root
		PS1="\u@$SHORTHOST \W# "
	else
		PS1="\u@$SHORTHOST \W\$ "
	fi
}
#..............................................................................
set_default_prompt() {
	if $(is_dullprompt_requested) ;
	then
		    dullprompt
	else
		case "$TERM" in
		    xterm-color|xterm-256color|rxvt*|screen-256color)
		    fancyprompt
		    ;;
		    *)
		    dullprompt
		    ;;
		esac
	fi
}
#..............................................................................
set_prompt() {
	if $(is_perforceprompt_requested); then
		perforce_prompt
	else
		set_default_prompt
	fi
}
#..............................................................................
set_history() {
	shopt -s histappend
	shopt -s cmdhist
	HISTIGNORE="&:ls:[bf]g:exit:[ \t]*"
	HISTFILESIZE=10000
	HISTSIZE=10000
}
#..............................................................................
export_xterm_256color_if_using_gnome_terminal() {
	if [ "$COLORTERM" == "gnome-terminal" ] && [ "$TERM" == "xterm" ]; then
		TERM="xterm-256color"
		export TERM
	fi
}
#..............................................................................
is_tmux_available() {
	if [ -n "$(type -P tmux | head -1)" ]; then
		return 0
	fi
	return 1
}
#..............................................................................
run_tmux() {
	if [ "$TERM" == "xterm-256color" ]; then
		TERM=screen-256color tmux -2 -u $@
	else
		tmux -2 -u $@
	fi
}
#..............................................................................
tmux_new_attach_if_term_ok() {
	if $(is_tmux_requested && is_tmux_available); then
		case "$TERM" in
			xterm-256color|screen-256color)
				if [ -z $TMUX ]; then
					if $(tmux has-session -t base); then
						run_tmux attach -t base
					else
						run_tmux new -s base
					fi
				fi
			;;
		esac
	fi
}
#..............................................................................
enable_utf8_lang_support() {
	LANG="en_US.utf8"
}
#================================= Script starts here =========================
get_dotfiles_dir
. "$RICVG_DOTFILES_DIR/sh/util.sh"
. "$RICVG_DOTFILES_DIR/sh/bash/perforce.sh"
get_options "${@}"
export_xterm_256color_if_using_gnome_terminal
set_prompt
set_history
tmux_new_attach_if_term_ok
enable_utf8_lang_support

alias ls="ls -F --color=auto"
alias ll="ls -l"
alias la="ls -A"

export PS1
export HISTIGNORE
export HISTFILESIZE
export HISTSIZE
export LANG
#=================================== Script Cleanup ===========================
unset opt_d_dullprompt
unset opt_perforce_prompt
unset -f get_options
unset -f is_dullprompt_requested
unset -f is_perforceprompt_requested
unset -f echo_absolute_path
unset -f get_dotfiles_dir
unset -f fancyprompt
unset -f dullprompt
unset -f set_prompt
unset -f set_history
unset -f export_xterm_256color_if_using_gnome_terminal
unset -f tmux_new_attach_if_term_ok
unset -f run_tmux
. "$RICVG_DOTFILES_DIR/sh/bash/perforce-cleanup.sh"
. "$RICVG_DOTFILES_DIR/sh/util-cleanup.sh"
