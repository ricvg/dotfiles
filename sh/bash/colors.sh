#!/bin/bash

# WARNING: Colors are wrong in Solaris 10's gnome terminal.
# TERM=xterm
# COLORTERM=gnome-terminal

for i in {0..255}; do echo -e "\e[38;05;${i}m${i}"; done | column -c 80 -s ' '; echo -e "\e[m"
