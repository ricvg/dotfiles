#!/bin/bash
#..............................................................................
. $RICVG_DOTFILES_DIR/sh/util.sh
#..............................................................................
echo_dirpath() {
	local dirname_color="$2"
	local basename_color="$3"
	local reset_color="$4"
	local sedregex_dirname='s/\/[^\/]*$//'
	local sedregex_basename='s/.*\/\([^\/]*\)$/\1/'
	echo "$dirname_color"'$(echo $(util_dir_chomp_no_tilda "${PWD}" '$1' ellipsis) | sed -e "'$sedregex_dirname'")/'"$basename_color"'$(echo $(util_dir_chomp_no_tilda "${PWD}" '$1' ellipsis) | sed -e "'$sedregex_basename'")'$reset_color
}
#..............................................................................
p4_prompt_fancy() {
	local p4w_max_len=16
	local middle_min_len=3
	local topline_max_midle_and_p4=`expr $p4w_max_len + $middle_min_len`
	local dirname_len_available='$(expr $COLUMNS - '"$topline_max_midle_and_p4"')'
	local dirname_color=$(util_fgcolor 243)
	local basename_color=$(util_fgcolor 247)
	if $(util_user_is_root); then
		local user_host_color=$(util_fgcolor 88)
	else
		local user_host_color=$dirname_color
	fi
	local reset_color=$(util_resetcolor)
	local dirpath_nocolor=$(echo_dirpath "$dirname_len_available")
	local dirpath_color=$(echo_dirpath "$dirname_len_available" "$(util_fgcolor 243)" "$(util_fgcolor 103)" "$(util_fgcolor 243)")
	local p4w='$(if [ "${#P4CLIENT}" -gt '"$p4w_max_len"' ]; then echo ${P4CLIENT:0:'$(expr $p4w_max_len - 1)'}'$(util_ellipsis)'; else echo $P4CLIENT; fi)'
	local p4="$p4w"
	local middle_empty_len='$((COLUMNS - $(expr "'"${dirpath_nocolor}"'" : ".*") - $(expr "'"${p4}"'" : ".*"))'
	local middle='$(for i in $(seq '"$middle_empty_len"')); do echo -n " "; done)'
	local topline="$dirpath_color$middle$p4"
	local underscore=$(util_fgcolor 233)'$(for i in $(seq $COLUMNS); do echo -n "_"; done)'
	PS1="${underscore}\n${topline}\n$user_host_color$USER@$SHORTHOST"
	if $(util_user_is_root); then
		PS1="${PS1}# "
	else
		PS1="${PS1}\$ "
	fi
	PS1="${PS1}$reset_color"
}
#..............................................................................
perforce_prompt() {
	if $(is_dullprompt_requested) ;
	then
		    dullprompt
	else
		case "$TERM" in
		    xterm-color|xterm-256color|rxvt*|screen-256color)
		    p4_prompt_fancy
		    ;;
		    *)
		    dullprompt
		    ;;
		esac
	fi
}
#=================================== Script Cleanup ===========================
# To clean up execute perforce-cleanup.sh
