#!/bin/bash
if [ "SunOS" != "$(uname)" ]; then
	return
fi
#..............................................................................
function _opencsw() {
	local fnc="$1"
	local app="$2"
	local args="$3"
	local cmd="$app $args"
	if [ ! -z $RICVG_DOTFILES_DEBUG ]; then
		echo "ricvg:$FUNCNAME:$1 => $cmd"
	fi
	eval "$cmd"
	return $?
}
export -f _opencsw
#..............................................................................
function awk() {
	_opencsw "$FUNCNAME" "$(type -p gawk gnuawk awk | head -1)" "$*"
	return $?
}
export -f awk
#..............................................................................
function base64() {
	_opencsw "$FUNCNAME" "$(type -p gbase64 base64 | head -1)" "$*"
	return $?
}
export -f base64
#..............................................................................
function basename() {
	_opencsw "$FUNCNAME" "$(type -p gbasename basename | head -1)" "$*"
	return $?
}
export -f basename
#..............................................................................
function cat() {
	_opencsw "$FUNCNAME" "$(type -p gcat cat | head -1)" "$*"
	return $?
}
export -f cat
#..............................................................................
function cp() {
	_opencsw "$FUNCNAME" "$(type -p gcp cp | head -1)" "$*"
	return $?
}
export -f cp
#..............................................................................
function cut() {
	_opencsw "$FUNCNAME" "$(type -p gcut cut | head -1)" "$*"
	return $?
}
export -f cut
#..............................................................................
function date() {
	_opencsw "$FUNCNAME" "$(type -p gdate date | head -1)" "$*"
	return $?
}
export -f date
#..............................................................................
function diff() {
	_opencsw "$FUNCNAME" "$(type -p gdiff diff | head -1)" "$*"
	return $?
}
export -f diff
#..............................................................................
function df() {
	echo "Calling df()"
	_opencsw "$FUNCNAME" "$(type -p gdf df | head -1)" "$*"
	return $?
}
export -f df
#..............................................................................
function du() {
	_opencsw "$FUNCNAME" "$(type -p gdu du | head -1)" "$*"
	return $?
}
export -f du
#..............................................................................
function false() {
	_opencsw "$FUNCNAME" "$(type -p gfalse false | head -1)" "$*"
	return $?
}
export -f false
#..............................................................................
function fmt() {
	_opencsw "$FUNCNAME" "$(type -p gdu du | head -1)" "$*"
	return $?
}
export -f fmt
#..............................................................................
#function head() {
#	_opencsw "$FUNCNAME" "$(type -p ghead head | head -1)" "$*"
#	return $?
#}
#export -f head
#..............................................................................
function join() {
	_opencsw "$FUNCNAME" "$(type -p gjoin join | head -1)" "$*"
	return $?
}
export -f join
#..............................................................................
function ls() {
	_opencsw "$FUNCNAME" "$(type -p gls ls | head -1)" "$*"
	return $?
}
export -f ls
#..............................................................................
function make() {
	_opencsw "$FUNCNAME" "$(type -p gmake make | head -1)" "$*"
	return $?
}
export -f make
#..............................................................................
function md5sum() {
	_opencsw "$FUNCNAME" "$(type -p gmd5sum md5sum | head -1)" "$*"
	return $?
}
export -f md5sum
#..............................................................................
function mv() {
	_opencsw "$FUNCNAME" "$(type -p gmv mv | head -1)" "$*"
	return $?
}
export -f mv
#..............................................................................
function nl() {
	_opencsw "$FUNCNAME" "$(type -p gnl nl | head -1)" "$*"
	return $?
}
export -f nl
#..............................................................................
function paste() {
	_opencsw "$FUNCNAME" "$(type -p gpaste paste | head -1)" "$*"
	return $?
}
export -f paste
#..............................................................................
function printf() {
	_opencsw "$FUNCNAME" "$(type -p gprintf printf | head -1)" "$*"
	return $?
}
export -f printf
#..............................................................................
function readlink() {
	_opencsw "$FUNCNAME" "$(type -p greadlink readlink | head -1)" "$*"
	return $?
}
export -f readlink
#..............................................................................
function rm() {
	_opencsw "$FUNCNAME" "$(type -p grm rm | head -1)" "$*"
	return $?
}
export -f rm
#..............................................................................
function seq() {
	_opencsw "$FUNCNAME" "$(type -p gseq seq | head -1)" "$*"
	return $?
}
export -f seq
#..............................................................................
function sha1sum() {
	_opencsw "$FUNCNAME" "$(type -p gsha1sum sha1sum | head -1)" "$*"
	return $?
}
export -f sha1sum
#..............................................................................
function sleep() {
	_opencsw "$FUNCNAME" "$(type -p gsleep sleep | head -1)" "$*"
	return $?
}
export -f sleep
#..............................................................................
function sort() {
	_opencsw "$FUNCNAME" "$(type -p gsort sort | head -1)" "$*"
	return $?
}
export -f sort
#..............................................................................
function split() {
	_opencsw "$FUNCNAME" "$(type -p gsplit split | head -1)" "$*"
	return $?
}
export -f split
#..............................................................................
function tac() {
	_opencsw "$FUNCNAME" "$(type -p gtac tac | head -1)" "$*"
	return $?
}
export -f tac
#..............................................................................
function tail() {
	_opencsw "$FUNCNAME" "$(type -p gtail tail | head -1)" "$*"
	return $?
}
export -f tail
#..............................................................................
function tar() {
	_opencsw "$FUNCNAME" "$(type -p gnutar gtar tar | head -1)" "$*"
	return $?
}
export -f tar
#..............................................................................
function tee() {
	_opencsw "$FUNCNAME" "$(type -p gtee tee | head -1)" "$*"
	return $?
}
export -f tee
#..............................................................................
function touch() {
	_opencsw "$FUNCNAME" "$(type -p gtouch touch | head -1)" "$*"
	return $?
}
export -f touch
#..............................................................................
function tr() {
	_opencsw "$FUNCNAME" "$(type -p gtr tr | head -1)" "$*"
	return $?
}
export -f tr
#..............................................................................
function true() {
	_opencsw "$FUNCNAME" "$(type -p gtrue true | head -1)" "$*"
	return $?
}
export -f true
#..............................................................................
function uniq() {
	_opencsw "$FUNCNAME" "$(type -p guniq uniq | head -1)" "$*"
	return $?
}
export -f uniq
#..............................................................................
function uptime() {
	_opencsw "$FUNCNAME" "$(type -p guptime uptime | head -1)" "$*"
	return $?
}
export -f uptime
#..............................................................................
function wc() {
	_opencsw "$FUNCNAME" "$(type -p gwc wc | head -1)" "$*"
	return $?
}
export -f wc
#..............................................................................
function who() {
	_opencsw "$FUNCNAME" "$(type -p gwho who | head -1)" "$*"
	return $?
}
export -f who
#..............................................................................
function whoami() {
	_opencsw "$FUNCNAME" "$(type -p gwhoami whoami | head -1)" "$*"
	return $?
}
export -f whoami
#..............................................................................
function yes() {
	_opencsw "$FUNCNAME" "$(type -p gyes yes | head -1)" "$*"
	return $?
}
export -f yes
#..............................................................................
alias tmux="TERM=screen-256color tmux -2"
