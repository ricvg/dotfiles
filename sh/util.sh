#!/bin/sh
#..............................................................................
util_echo_absolute_path() {
	local path="$1"

	while [ -L "$path" ]; do
		dir=`dirname "$path"`
		path=`ls -l "$path" | sed -e 's/.* -> //'`
		cd "$dir"
	done

	local file=`basename "$path"`
	local dir=""

	if [ ! -d "$file" ] ; then
		# $file is NOT a directory, then split the path into dir and file.
		dir=`dirname "$path"`
	else
		# $file is a directory, then clear $file since is is not useful.
		file=""
		dir="$path"
	fi

	if [ ! -d "$dir" ]; then
		return 1
	fi

	local abs_name="`cd "$dir" && pwd -P`"

	if [ -n "$file" ]; then
		abs_name=$abs_name/$file
	fi
	echo $abs_name
}
#..............................................................................
util_echo_userid() {
	local userid=""
	if [ "SunOS" == "$(uname)" ] && [ "5.10" == "$(uname -r)" ]; then
		userid=`id | cut -d '=' -f2 | cut -d '\(' -f1`
	else
		userid=`id | cut -d '=' -f2 | cut -d '(' -f1`
	fi
	echo $userid
}
#..............................................................................
util_user_is_root() {
	if [ "$(util_echo_userid)" == "0" ]; then
		return 0
	fi

	return 1
}
#..............................................................................
util_boldtext() {
	echo "\\[\\033[1m\\]"$1"\\[\\033[0m\\]"
}
#..............................................................................
util_bgcolor() {
	echo "\\[\\033[48;5;"$1"m\\]"
}
#..............................................................................
util_fgcolor() {
	echo "\\[\\033[38;5;"$1"m\\]"
}
#..............................................................................
util_resetcolor() {
	echo "\\[\\e[0m\\]"
}
#..............................................................................
util_ellipsis() {
	echo -e "\xE2\x80\xA6"
}
#..............................................................................
util_dir_chomp_no_tilda() {
	local append_ellipsis=$3
	local p="$1" b s
	s=${#p}
	while [[ $p != ${p//\/} ]] && (($s>$2))
	do
		p=${p#/}
		[[ $p =~ \.?. ]]
		b=$b/${BASH_REMATCH[0]}
		if [ "$append_ellipsis" == "ellipsis" ]
		then
			b="$b$(echo -e "\xE2\x80\xA6")"
		fi
		p=${p#*/}
		((s=${#b}+${#p}))
	done
	echo ${b/\/~/\~}${b+/}$p
}
#..............................................................................
util_dir_chomp() {
	local append_ellipsis=$3
	# Retrieved from http://stackoverflow.com/a/3498912/285423
	local p=${1/#$HOME/\~} b s
	s=${#p}
	while [[ $p != ${p//\/} ]] && (($s>$2))
	do
		p=${p#/}
		[[ $p =~ \.?. ]]
		b=$b/${BASH_REMATCH[0]}
		if [ -n $append_ellipsis ] && [ "/~" != "$b" ]
		then
			b="$b$(echo -e "\xE2\x80\xA6")"
		fi
		p=${p#*/}
		((s=${#b}+${#p}))
	done
	echo ${b/\/~/\~}${b+/}$p
}
#..............................................................................
