#!/bin/sh
unset -f util_echo_absolute_path
unset -f util_echo_userid
unset -f util_user_is_root
unset -f util_boldtext
unset -f util_bgcolor
unset -f util_fgcolor
unset -f util_resetcolor
