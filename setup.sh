#!/bin/bash
DEBUG=1
# ============================== Script Functions =============================
printdbg() {
	if [ 1 -eq $DEBUG ]; then
		echo -e "ricvg:debug:$@"
	fi
}
#..............................................................................
ABS_NAME=""
get_abs_name() {
	local path="$1"

	while [ -L "$path" ]; do
		dir=`dirname "$path"`
		path=`ls -l "$path" | sed -e 's/.* -> //'`
		cd "$dir"
	done

	dir=`dirname "$path"`
	file=`basename "$path"`
	if [ ! -d "$dir" ]; then
		echo "canonize: $dir: No such directory" >&2
		exit 1
	fi
	ABS_NAME="`cd "$dir" && pwd -P`/$file"
}
#..............................................................................
RICVG_DOTFILES_DIR=''
get_dotfiles_dir() {
	get_abs_name "${BASH_SOURCE[0]}"
	RICVG_DOTFILES_DIR="$(dirname $ABS_NAME)"
}
#..............................................................................
BASH_PROFILE="$HOME/.bash_profile"
BASHRC="$HOME/.bashrc"
DOT_PROFILE="$HOME/.profile"
TARGET_PROFILE=''
select_bash_profile_file() {
	if [ -f "$BASHRC" ]; then
		TARGET_PROFILE="$BASHRC"
	elif [ -f "$BASH_PROFILE" ]; then
		TARGET_PROFILE="$BASH_PROFILE"
	else
		TARGET_PROFILE=$DOT_PROFILE
	fi
}
#..............................................................................
#ABS_NAME=""
#	local target="$1"
#	if [ -z $target ]; then
#		echo -e "error: get_abs_name() needs input argument."
#		exit 1
#	fi
#	local target_basename=$(basename $target)
#	local target_dirname=$(dirname $target)
#	ABS_NAME=`cd $target_dirname; pwd`/$target_basename
#}
#..............................................................................
create_symlink_to_file() {
	local target="$1"
	local symlink="$2"
	if [ -z "$target" ] || [ -z "$symlink" ]; then
		echo -e "error: create_symlink_to_file() needs a target and/or symlink"
		exit 1
	fi

	if [ ! -f "$target" ] && [ ! -d "$target" ]; then
		echo -e "error: create_symlink_to_file() cannot create symbolic link \"$symlink\" because the target \"$target\" doesn't exists"
		exit 1
	fi

	if [ -L "$symlink" ]; then
		get_abs_name "$symlink"
		local symlink_target_path=$ABS_NAME
		get_abs_name "$target"
		local target_path=$ABS_NAME
		if [ $target_path != $symlink_target_path ]; then
			echo -e "warning: create_symlink_to_file() cannot" \
			        "create symbolic link \"$symlink\" because" \
			        "a symbolic link with the same name already" \
			        "exists but points to" \
			        "\"$symlink_target_path\" instead of the" \
			        "intended target \"$target_path\"."
		fi
		return
	fi

	if [ -f "$symlink" ] || [ -d "$symlink" ]; then
		echo -e "warning: create_symlink_to_file() cannot create symbolic link \"$symlink\" because a file or directory with the same name already exists."
		return
	fi

	ln -s "$target" "$symlink"
}
#..............................................................................
create_symlink_to_dir() {
	local target_dir=$1
	local symlink=$2
	if [ -z "$target_dir" ] || [ -z "$symlink" ]; then
		echo -e "error: create_symlink_to_dir() needs target directory and/or symlink"
		exit 1
	fi

	if [ ! -d "$target_dir" ]; then
		echo -e "error: create_symlink_to_dir() cannot create symbolic link \"$symlink\" because target directory \"$target_dir\" doesn't exists."
		exit 1
	fi

	if [ -L "$symlink" ]; then
		echo -e "warning: create_symlink_to_dir() cannot create symbolic link \"$symlink\" because a symbolic link with the same name already exists."
		return
	fi

	if [ -d "$symlink" ]; then
		echo -e "warning: create_symlink_to_dir() cannot create symbolic link \"$symlink\" because a directory with same name already exists."
		return
	fi

	ln -s "$target_dir" "$symlink"
}
#..............................................................................
append_script_sourcing_to_script() {
	local script=$1
	local script_opts="$3"
	local appended_script=$2
	printdbg "$FUNCNAME: script=$script"
	printdbg "$FUNCNAME: appented_script=$appended_script"
	if [ -z $script ] || [ -z $appended_script ]; then
		echo -e "error: append_script_sourcing_to_script() needs target script and/or file to append to."
		exit 1
	fi
	if [ -f $appended_script ]; then
		local target_basename=$( basename $script )
		local grep_result=`grep -c $target_basename $appended_script`
		printdbg "$FUNCNAME: target_basename=$target_basename"
		printdbg "$FUNCNAME: grep says $target_basename is $grep_result times in $appended_script"
		if [ 0 -eq $(grep -c $target_basename $appended_script) ] ; then
			echo "Appending script \"$script\" to \"$appended_script\""
			local txt_2_append="\n# Added by $RICVG_DOTFILES_DIR/setup.sh\nif [ -f $script ]; then\n\t. $script $script_opts\nfi\n"
			echo -e "$txt_2_append" >> $appended_script
		else
			printdbg "$FUNCNAME: Skipping appending \"$target_basename\""
		fi
	fi
}
#..............................................................................
append_shorthost_to_profile() {
	append_script_sourcing_to_script "$RICVG_DOTFILES_DIR/sh/shorthost.sh" "$TARGET_PROFILE"
}
#..............................................................................
is_sunos() {
	if [ "SunOS" == "$(uname)" ]; then
		return 0
	fi
	return 1
}
#..............................................................................
append_mybashrc_to_profile() {
	if [ "$TARGET_PROFILE" == "$DOT_PROFILE" ]; then
		echo -e "if [ -n "$BASH_VERSION" ]; then" >> "$TARGET_PROFILE"
	fi

	append_script_sourcing_to_script "$RICVG_DOTFILES_DIR/sh/bash/mybashrc.sh" "$TARGET_PROFILE" "\$RICVG_OPTS"

	if [ "$TARGET_PROFILE" == "$DOT_PROFILE" ]; then
		echo -e "fi" >> "$TARGET_PROFILE"
	fi
}
#..............................................................................
create_symlink_to_tmuxconf() {
	create_symlink_to_file $RICVG_DOTFILES_DIR/tmux/tmux.conf $HOME/.tmux.conf
}
#..............................................................................
create_symlink_to_dotvim() {
	create_symlink_to_file "$RICVG_DOTFILES_DIR/vim" "$HOME/.vim"
}
#..............................................................................
create_symlink_to_defaultvimrc() {
	create_symlink_to_file $RICVG_DOTFILES_DIR/vim/default.vim $HOME/.vimrc
}
#..............................................................................
create_symlink_to_screenconf() {
	create_symlink_to_file $RICVG_DOTFILES_DIR/screen/screen.conf $HOME/.screenrc
}
#..............................................................................
create_symlink_to_gitconfig() {
	create_symlink_to_file $RICVG_DOTFILES_DIR/git/gitconfig $HOME/.gitconfig
}
#..............................................................................
create_symlink_to_awesome_rc() {
	mkdir -p $HOME/.config/awesome
	create_symlink_to_file $RICVG_DOTFILES_DIR/awesome/rc.lua $HOME/.config/awesome/rc.lua
}
#..............................................................................
create_symlink_to_xinitrc() {
	create_symlink_to_file $RICVG_DOTFILES_DIR/xinitrc $HOME/.xinitrc
}
#..............................................................................
create_symlink_to_Xmodmap() {
	create_symlink_to_file $RICVG_DOTFILES_DIR/Xmodmap $HOME/.Xmodmap
}
# ============================== Script Body Start ============================
get_dotfiles_dir
select_bash_profile_file
append_shorthost_to_profile
append_mybashrc_to_profile
create_symlink_to_dotvim
create_symlink_to_tmuxconf
create_symlink_to_defaultvimrc
create_symlink_to_screenconf
create_symlink_to_gitconfig
create_symlink_to_awesome_rc
create_symlink_to_xinitrc
create_symlink_to_Xmodmap
